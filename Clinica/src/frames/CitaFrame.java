package frames;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JButton;
import javax.swing.JPanel;

import complementos.LogoFrame;
import informacion.Cita;
import paneles.Imprimible;

public class CitaFrame extends LogoFrame implements ActionListener,Printable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Imprimible recibo;
	JButton boton;
	JPanel panel;
	public CitaFrame(Cita cita){
		super("Imprimir cita");
		getContentPane().setLayout(new BorderLayout());
		panel=new JPanel();
		boton=new JButton();
		boton.setText("Imprimir");
		boton.addActionListener(this);
		panel.add(boton);
		recibo=new Imprimible(cita);
		this.getContentPane().add(panel,BorderLayout.SOUTH);
		this.getContentPane().add(recibo,BorderLayout.CENTER);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(boton)){
	        try {             
	        	PrinterJob job = PrinterJob.getPrinterJob();        
	        	job.setPrintable(this);
	        	job.printDialog();
	        	job.print();
	        	}catch (PrinterException ex){ }
	        }
	}
		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
			// TODO Auto-generated method stub
			if (pageIndex > 0) return NO_SUCH_PAGE;
				Graphics2D g2d = (Graphics2D)graphics;
				//Punto donde empezar� a imprimir dentro la pagina (100, 50)
				g2d.translate(pageFormat.getImageableX()+100, pageFormat.getImageableY()+50);
				g2d.scale(0.50,0.50);
				//Reducci�n de la impresi�n al 50%
				recibo.printAll(graphics);
				return PAGE_EXISTS;
		}
	}
