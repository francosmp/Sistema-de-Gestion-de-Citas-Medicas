package frames;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import complementos.Logo;
import complementos.MiFrame;
import login.Login;
import javax.swing.JButton;
public class MenuRecepcionista extends MiFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btnBuscarCita;
	JButton btnRegistrar ;
	public MenuRecepcionista(){
		super("Bienvenido");
		Salir.addActionListener(this);
		CerrarSesion.addActionListener(this);
		this.setSize(400,333);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		Logo logo=new Logo();
		logo.setSize(300, 120);
		logo.setLocation(45, 10);
		this.getContentPane().add(logo);
		
		btnRegistrar = new JButton("Registrar Paciente");
		btnRegistrar.addActionListener(this);
		btnRegistrar.setBounds(122, 141, 138, 23);
		getContentPane().add(btnRegistrar);
		
		btnBuscarCita = new JButton("Buscar Cita");
		btnBuscarCita.setBounds(122, 174, 138, 23);
		getContentPane().add(btnBuscarCita);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnRegistrar)){
			new RegistrarPaciente();
		}
		else if(e.getSource().equals(btnBuscarCita)){
			new BuscarCita();
		}
		else if(e.getSource().equals(Salir)){
			System.exit(0);
		}
		else if(e.getSource().equals(CerrarSesion)){
			this.dispose();
			new Login();
		}
	}
}
