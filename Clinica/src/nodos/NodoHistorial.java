package nodos;

import informacion.Historia;

public class NodoHistorial {
	private Historia historia;
	private NodoHistorial next;
	public NodoHistorial(Historia historia){
		this.setHistoria(historia);
		this.setNext(null);
	}
	public Historia getHistoria() {
		return historia;
	}
	public void setHistoria(Historia historia) {
		this.historia = historia;
	}
	public NodoHistorial getNext() {
		return next;
	}
	public void setNext(NodoHistorial next) {
		this.next = next;
	}
}

