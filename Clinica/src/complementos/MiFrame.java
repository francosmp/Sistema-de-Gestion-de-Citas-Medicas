package complementos;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
public class MiFrame extends LogoFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuItem Salir=null;
	protected JMenuItem CerrarSesion=null;
	public MiFrame(String nombre){
		super(nombre);
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu mnNewMenu = new JMenu("Opciones");
		menuBar.add(mnNewMenu);
		Salir = new JMenuItem("Salir");
		CerrarSesion = new JMenuItem("Cerrar Sesi�n");
		mnNewMenu.add(CerrarSesion);
		mnNewMenu.add(Salir);
	}
}
