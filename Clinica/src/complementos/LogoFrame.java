package complementos;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
public class LogoFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogoFrame(String titulo){
		super(titulo);
		setIconImage(new ImageIcon(getClass().getResource("/Imagenes/miniatura.jpg")).getImage());
	}
}
