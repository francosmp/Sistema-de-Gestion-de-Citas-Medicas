package complementos;

import frames.RegistrarPaciente;
import informacion.Cita;
import informacion.Medico;
import informacion.Persona;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

public class registrarBasedatos {

    public boolean registrarPaciente(Persona p) {
        Connection conectar = Conexion.getConexionMYSQL();
        boolean estado;
        if (p != null) {
            try {
                PreparedStatement ps = conectar.prepareStatement("insert into clinica.persona values(?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, p.getCodigop());
                ps.setString(2, p.getNombre());
                ps.setString(3, p.getApellido());
                ps.setInt(4, p.getEdad());
                ps.setString(5, p.getSexo());
                ps.setDate(6, p.getFechaNac());
                ps.setString(7, p.getTelefono());
                ps.setString(8, p.getNacionalidad());
                ps.setString(9, p.getDireccion());
                ps.setString(10, p.getEstadoCivil());
                ps.setString(11, p.getOcupacion());
                ps.setString(12, p.getDni());
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            }
            return estado = true;
        } else {
            System.out.println("lista vacia");
            estado = false;
        }
        return estado;
    }

    public boolean RegistrarCita(Cita cita) {
        Connection conectar = Conexion.getConexionMYSQL();
        boolean estado;
        if (cita != null) {
            try {
                PreparedStatement ps = conectar.prepareStatement("insert into clinica.citas values(?,?,?,?,?,?,?,?)");
                ps.setString(1, cita.getCodigo());
                ps.setString(2, cita.getPaciente().getCodigop());
                ps.setString(3, cita.getMedico().getCodigoM());
                ps.setDate(4, cita.getFechaCita());
                ps.setTime(5, cita.getHoraCita());
                ps.setString(6, cita.getAcceso());
                ps.setString(7, cita.getDescripcion());
                ps.setInt(8, 0);
                ps.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            }
            estado = true;
        } else {
            System.out.println("error vacia");
            estado = false;
        }
        return estado;
    }

    public void eliminarCita(String codigo,String codigoM,Date fechaC,String codigoH) {
        Connection conectar = Conexion.getConexionMYSQL();
        String cadena="cita cancelada";
        try {
            PreparedStatement ps=conectar.prepareStatement("update clinica.citas set citas.descripcion where citas.codigoCita=?;");
            ps.setString(1,cadena);//ojo
            ps.setString(2,codigo);
             int rpt=ps.executeUpdate();
             if(rpt>0){
                 System.out.println("correcto");
             }else{
                 System.out.println("incorrecto");
             }
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            PreparedStatement pss = conectar.prepareStatement("delete from clinica.turnos where turnos.codigoMedico=? and turnos.codigoHora=? and turnos.dia=?");
            pss.setString(1,codigoM);
            pss.setString(2,codigoH);
            pss.setDate(3,fechaC);
            int m = pss.executeUpdate();
            if (m > 0) {
                System.out.println("correcto");
            } else {
                System.out.println("error turnos");
            }
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean RegistrarTurno(String CodigoM, String codigoH, Date FechaCita) {
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            PreparedStatement ps = conectar.prepareStatement("insert into clinica.turnos values(?,?,?,?)");
            ps.setString(1, CodigoM);
            ps.setString(2, codigoH);
            ps.setInt(3, 9);//el ID de donde sale 
            ps.setDate(4, FechaCita);
            return (ps.executeUpdate() > 0);
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean verificarHorario(String codigo, Date fec, String codigoH) {
        String sql = "select count(*) from clinica.turnos where (turnos.codigoMedico=" + "'" + codigo + "'" + " and turnos.dia=" + "'" + fec + "'" + " and turnos.codigoHora=" + "'" + codigoH + "')";
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            Statement st = conectar.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            int n = rs.getInt("count(*)");
            rs.beforeFirst();
            return n > 0;
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean verificarFecha(String codigo, Date fecha, int horasT) {
        String sql = "select count(*) from clinica.turnos where (turnos.codigoMedico=" + "'" + codigo + "'" + " and turnos.dia=" + "'" + fecha + "')";
        Connection conectar = Conexion.getConexionMYSQL();
        try {
            Statement st = conectar.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            int n = rs.getInt("count(*)");
            return n > horasT;
        } catch (SQLException ex) {
            Logger.getLogger(registrarBasedatos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
