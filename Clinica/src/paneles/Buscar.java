package paneles;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class Buscar extends JPanel implements ActionListener {
	JButton btnBuscar;
	TablaAcceso tabla;
	boolean flag;
	public Buscar() {
		setLayout(new BorderLayout(0, 0));
		flag=false;
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		
		JLabel lblDni = new JLabel("Codigo del M\u00E9dico:");
		panel.add(lblDni);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		textField.addActionListener(this);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this);
		panel.add(btnBuscar);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().equals(btnBuscar) || e.getSource().equals(textField)){
			if(!textField.getText().equals("")){
				if(flag){
					this.remove(tabla);
				}
				tabla=new TablaAcceso(textField.getText());
				this.add(tabla,BorderLayout.CENTER);
				this.updateUI();
				flag=true;
			}
		}
	}

}
