package paneles;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import complementos.Conexion;
import listas.Historiales;
public class PanelHistorial extends JPanel implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JLabel nombre;
	public JLabel talla;
	public JLabel peso;
	public JLabel tipoSangre;
	public JLabel edad;
	public JLabel sexo;
	public JLabel alergias;
	public JLabel dni;
	public JPanel pnl;
	public JDBTableO table;
	public JScrollPane scroll;
	public Historiales historias;
	public String codigoMedico;
	public String codigoPaciente;
	private Connection cn;
	public PanelHistorial(String codigoMedico){
		super();
		this.codigoMedico=codigoMedico;
		codigoPaciente=null;
		this.setLayout(new BorderLayout());
		cn=Conexion.getConexionMYSQL();
	}
	private void verificar() {
		// TODO Auto-generated method stub

		try{
			Statement st=cn.createStatement();
			ResultSet rs= st.executeQuery("select * from clinica.temporal where codigoMedico='"+codigoMedico+"';");
			if(rs.next()){
				if(!rs.getString("codigoPaciente").equals(codigoPaciente)){
				this.removeAll();
				init(rs.getString("codigoPaciente"));
				codigoPaciente=rs.getString("codigoPaciente");
				this.updateUI();
				}
			}
			else{
				this.removeAll();
				this.add(new JLabel("no hay paciente",JLabel.CENTER),BorderLayout.CENTER);
				this.updateUI();
			}
			st.close();
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private void init(String codigoPaciente) {
		// TODO Auto-generated method stub
		historias=new Historiales(codigoPaciente);
		pnl=new JPanel();
		pnl.setLayout(new GridLayout(4,2));
		nombre=new JLabel("Nombre: "+historias.getPaciente().getNombre()+" "+historias.getPaciente().getApellido()+" ");
		pnl.add(nombre);
		dni=new JLabel("DNI: "+historias.getPaciente().getDni());
		pnl.add(dni);
		edad=new JLabel("Edad: "+String.valueOf(historias.getPaciente().getEdad()));
		pnl.add(edad);
		sexo=new JLabel("Sexo: "+historias.getPaciente().getSexo());
		pnl.add(sexo);
		talla=new JLabel("Talla: "+String.valueOf(historias.getTalla()));
		pnl.add(talla);
		peso=new JLabel("Peso: "+String.valueOf(historias.getPeso()));
		pnl.add(peso);
		tipoSangre=new JLabel("Tipo de Sangre: "+historias.getTipoSangre());
		pnl.add(tipoSangre);
		this.alergias=new JLabel("Alergias: "+historias.getAlergias());
		pnl.add(alergias);
		this.add(pnl,BorderLayout.NORTH);
		table=new JDBTableO(historias);
		this.add(table,BorderLayout.CENTER);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			verificar();
		}
	}
}
